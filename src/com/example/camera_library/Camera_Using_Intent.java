package com.example.camera_library;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class Camera_Using_Intent extends ActionBarActivity implements OnClickListener 
{

	ImageView image;
	Button camera,next;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		image=(ImageView)findViewById(R.id.image);
		
		camera=(Button)findViewById(R.id.btncamera);
		camera.setOnClickListener(this);	
		
		next=(Button)findViewById(R.id.btnnext);
		next.setOnClickListener(this);	
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) 
	{
		if(v.getId()==R.id.btncamera)
		{
			Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, 0);
		}

		if(v.getId()==R.id.btnnext)
		{
			Intent intent = new Intent(this,Camera_Using_Api.class);
           startActivity(intent);
		}		
	}	
	 protected void onActivityResult(int requestCode, int resultCode, Intent data)
	 {
	      super.onActivityResult(requestCode, resultCode, data);
	      Bitmap bp = (Bitmap) data.getExtras().get("data");
	      image.setImageBitmap(bp);
	 }
}
