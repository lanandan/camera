package com.example.camera_library;

import android.support.v7.app.ActionBarActivity;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;
public class Camera_Using_Api extends ActionBarActivity implements OnClickListener 
{

	private ImageSurfaceView mImageSurfaceView;
    private Camera camera; 
    private FrameLayout cameraPreviewLayout;
    private ImageView capturedImageHolder;
	private Button capture;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camera__using__api);
		
		
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		 
        cameraPreviewLayout = (FrameLayout)findViewById(R.id.camera_preview);
       // capturedImageHolder = (ImageView)findViewById(R.id.captured_image);
        capture=(Button)findViewById(R.id.btncapture);
        capture.setOnClickListener(this);
        
        
        camera = checkDeviceCamera();
        mImageSurfaceView = new ImageSurfaceView(this, camera);
        cameraPreviewLayout.addView(mImageSurfaceView);
 
       
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.camera__using__api, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private Camera checkDeviceCamera(){
        Camera mCamera = null;
        try {
            mCamera = Camera.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mCamera;
    }
 
	
	
	@Override
	public void onClick(View v) 
	{
	
		if(v.getId()==R.id.btncapture)
		{
			camera.takePicture(null, null, pictureCallback);
		}	
	}
	
	
	PictureCallback pictureCallback = new PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
            if(bitmap==null){
                Toast.makeText(getApplicationContext(), "Captured image is empty", Toast.LENGTH_LONG).show();
                return;
            }
            //capturedImageHolder.setImageBitmap(scaleDownBitmapImage(bitmap, 300, 200 ));            
           // capturedImageHolder.setImageBitmap(bitmap);
            
        }
    };
	
    private Bitmap scaleDownBitmapImage(Bitmap bitmap, int newWidth, int newHeight){
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
        return resizedBitmap;
    }
	
	
	
	
	
}
